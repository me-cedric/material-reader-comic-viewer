import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { HomeComponent } from './home/home.component'
import { SearchComponent } from './search/search.component'
import { HistoryComponent } from './history/history.component'
import { BookmarksComponent } from './bookmarks/bookmarks.component'
import { AboutComponent } from './about/about.component'
import { ReaderComponent } from './reader/reader.component'

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'search', component: SearchComponent },
  { path: 'history', component: HistoryComponent },
  { path: 'bookmarks', component: BookmarksComponent },
  { path: 'about', component: AboutComponent },
  { path: 'reader/:url',      component: ReaderComponent },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
