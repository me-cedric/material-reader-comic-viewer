import { Component } from '@angular/core'

import { WindowService } from './services/window.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isExpanded = false
  private os = null
  constructor (private windowService: WindowService) {
    this.windowService.getOs()
    .then(os => {
      this.os = os
    })
  }
  minimize () {
    console.log('minimize')
    this.windowService.minimize()
  }
  maximize () {
    this.windowService.maximize()
  }
  close () {
    this.windowService.close()
  }
}
