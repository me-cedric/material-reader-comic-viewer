import { Injectable } from '@angular/core'
import { IpcRenderer } from 'electron'
import { v4 } from 'uuid'
import { FileElement } from '../model/file-element/file-element'
import { SettingFile } from '../model/setting-file/setting-file'
import { BehaviorSubject, Observable } from 'rxjs'

export interface IFileService {
  add(fileElement: FileElement)
  delete(id: string)
  update(id: string, update: Partial<FileElement>)
  queryInFolder(folderId: string): Observable<FileElement[]>
  get(id: string): FileElement
}

@Injectable({
  providedIn: 'root'
})
export class FileService implements IFileService {
  private ipc: IpcRenderer
  private map = new Map<string, FileElement>()
  private userSettings: SettingFile

  constructor() {
    if ((<any>window).require) {
      try {
        this.ipc = (<any>window).require('electron').ipcRenderer
      } catch (error) {
        throw error
      }
    } else {
      console.warn('Could not load electron ipc')
    }
    this.getUserSettings()
      .then((settings) => {
        this.userSettings = settings
      })
  }

  /**
   * Get a list of files in the folder from its path
   */
  async getFiles(path) {
    return new Promise<string[]>((resolve, reject) => {
      this.ipc.once('getFilesResponse', (event, arg) => {
        resolve(arg)
      })
      this.ipc.send('getFiles', path)
    })
  }

  /**
   * Get a file from path
   */
  async getFile(path) {
    return new Promise<string[]>((resolve, reject) => {
      this.ipc.once('getFileResponse', (event, arg) => {
        resolve(arg)
      })
      this.ipc.send('getFile', path)
    })
  }

  /**
   * Get a file stats from path
   */
  async getFileStats(dir, name) {
    return new Promise<string[]>((resolve, reject) => {
      this.ipc.once('getFileStatsResponse', (event, arg) => {
        resolve(arg)
      })
      this.ipc.send('getFileStats', { directory: dir, filename: name })
    })
  }

  /**
   * Get multiple file stats from path
   */
  async getFilesStats(pathInfoArray) {
    return new Promise<string[]>((resolve, reject) => {
      this.ipc.once('getFilesStatsResponse', (event, arg) => {
        resolve(arg)
      })
      this.ipc.send('getFilesStats', pathInfoArray)
    })
  }

  /**
   * Get multiple file stats from path
   */
  async getUserSettings() {
    return new Promise<SettingFile>((resolve, reject) => {
      this.ipc.once('getUserSettingsResponse', (event, arg) => {
        resolve(arg)
      })
      this.ipc.send('getUserSettings')
    })
  }

  /**
   * Get multiple file stats from path
   */
  async getDirectoryFilesAndStats(dirPath) {
    return new Promise<string[]>((resolve, reject) => {
      this.getFiles(dirPath)
        .then(files => {
          let filePathInfo = []
          console.log(files)
          files.forEach((fileName) => {
            filePathInfo.push({ directory: dirPath, filename: fileName })
          })
          return this.getFilesStats(filePathInfo)
        })
        .then((fileStats) => {
          resolve(fileStats)
        })
    })
  }

  add(fileElement: FileElement) {
    fileElement.id = v4()
    this.map.set(fileElement.id, this.clone(fileElement))
    return fileElement
  }

  delete(id: string) {
    this.map.delete(id)
  }

  update(id: string, update: Partial<FileElement>) {
    let element = this.map.get(id)
    element = Object.assign(element, update)
    this.map.set(element.id, element)
  }

  private querySubject: BehaviorSubject<FileElement[]>
  queryInFolder(folderId: string) {
    const result: FileElement[] = []
    this.map.forEach(element => {
      if (element.parent === folderId) {
        result.push(this.clone(element))
      }
    })
    if (!this.querySubject) {
      this.querySubject = new BehaviorSubject(result)
    } else {
      this.querySubject.next(result)
    }
    return this.querySubject.asObservable()
  }

  get(id: string) {
    return this.map.get(id)
  }

  clone(element: FileElement) {
    return JSON.parse(JSON.stringify(element))
  }
}