import { Injectable } from '@angular/core'
import { IpcRenderer } from "electron"

@Injectable({
  providedIn: 'root'
})
export class WindowService {
  private ipc: IpcRenderer

  constructor () {
    if ((<any>window).require) {
      try {
        this.ipc = (<any>window).require("electron").ipcRenderer
      } catch (error) {
        throw error
      }
    } else {
      console.warn("Could not load electron ipc")
    }
  }

  async minimize () {
    this.ipc.send("minimize")
  }

  async maximize () {
    this.ipc.send("maximize")
  }

  async close () {
    this.ipc.send("close")
  }

  async getOs() {
    return new Promise<string[]>((resolve, reject) => {
      this.ipc.once("getOsResponse", (event, arg) => {
        resolve(arg)
      })
      this.ipc.send("getOs")
    })
  }
}
