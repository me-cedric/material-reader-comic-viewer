import { Keymap } from '../keymap/keymap'

export class SettingFile { // mirrored in electron models
    comicFolders: Array<string>
    keymap: Keymap
}

const DefaultSettings: SettingFile = {
    comicFolders: [],
    keymap: {
        previousPage: '',
        nextPage: '',
        zoomIn: '',
        zoomOut: '',
        search: ''
    }
}

export {DefaultSettings}