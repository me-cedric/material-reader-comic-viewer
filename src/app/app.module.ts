import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import {
  MatToolbarModule,
  MatIconModule,
  MatSidenavModule,
  MatListModule,
  MatDividerModule,
  MatGridListModule,
  MatMenuModule,
  MatDialogModule,
  MatInputModule,
  MatButtonModule 
} from '@angular/material'

import { FormsModule } from '@angular/forms'

import { HomeComponent } from './home/home.component'
import { SearchComponent } from './search/search.component'
import { HistoryComponent } from './history/history.component'
import { BookmarksComponent } from './bookmarks/bookmarks.component'
import { SettingsComponent } from './settings/settings.component'
import { AboutComponent } from './about/about.component'
import { ReaderComponent } from './reader/reader.component'
import { FileExplorerComponent } from './file-explorer/file-explorer.component';
import { NewFolderDialogComponent } from './file-explorer/modals/new-folder-dialog/new-folder-dialog.component';
import { RenameDialogComponent } from './file-explorer/modals/rename-dialog/rename-dialog.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    HistoryComponent,
    BookmarksComponent,
    SettingsComponent,
    AboutComponent,
    ReaderComponent,
    FileExplorerComponent,
    NewFolderDialogComponent,
    RenameDialogComponent
  ],
  exports: [FileExplorerComponent],
  entryComponents: [NewFolderDialogComponent, RenameDialogComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatMenuModule,
    MatDividerModule,
    MatGridListModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
