import { Component, OnInit } from '@angular/core'

import { FileService } from '../services/file.service'
import { FileElement } from '../model/file-element/file-element'
import { Observable } from 'rxjs'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public fileElements: Observable<FileElement[]>

  constructor (private fileService: FileService) {}

  currentRoot: FileElement
  currentPath: string
  canNavigateUp = false

  ngOnInit () {
    this.updateFileElementQuery()
  }

  addFolder(folder: { name: string }) {
    this.fileService.add({ isFolder: true, name: folder.name, parent: this.currentRoot ? this.currentRoot.id : 'root' })
    this.updateFileElementQuery()
  }

  removeElement(element: FileElement) {
    this.fileService.delete(element.id)
    this.updateFileElementQuery()
  }

  navigateToFolder(element: FileElement) {
    this.currentRoot = element
    this.updateFileElementQuery()
    this.currentPath = this.pushToPath(this.currentPath, element.name)
    this.canNavigateUp = true
  }

  navigateUp() {
    if (this.currentRoot && this.currentRoot.parent === 'root') {
      this.currentRoot = null
      this.canNavigateUp = false
      this.updateFileElementQuery()
    } else {
      this.currentRoot = this.fileService.get(this.currentRoot.parent)
      this.updateFileElementQuery()
    }
    this.currentPath = this.popFromPath(this.currentPath)
  }

  moveElement(event: { element: FileElement; moveTo: FileElement }) {
    this.fileService.update(event.element.id, { parent: event.moveTo.id })
    this.updateFileElementQuery()
  }

  renameElement(element: FileElement) {
    this.fileService.update(element.id, { name: element.name })
    this.updateFileElementQuery()
  }

  updateFileElementQuery() {
    this.fileElements = this.fileService.queryInFolder(this.currentRoot ? this.currentRoot.id : 'root')
  }

  pushToPath(path: string, folderName: string) {
    let p = path ? path : ''
    p += `${folderName}/`
    return p
  }

  popFromPath(path: string) {
    let p = path ? path : ''
    let split = p.split('/')
    split.splice(split.length - 2, 1)
    p = split.join('/')
    return p
  }
}
