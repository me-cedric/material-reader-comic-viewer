import { OsFinder } from './os'
import * as path from 'path'
import * as url from 'url'
import {
  app,
  shell,
  BrowserWindow,
  WebPreferences
} from 'electron'
import { MenuBuilder } from './menu'
import { FileListener } from './file'

let mainWindow: BrowserWindow | null = null
const currentOs: string = OsFinder.current()
const webPreferences: WebPreferences = { nodeIntegration: true }
const userData: string = app.getAppPath() // todo: use getpath('userData') for AppData folder

app.on('ready', createWindow)

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  app.quit()
})

async function createWindow () {
  mainWindow = new BrowserWindow({
    show: false,
    width: 1104,
    height: 720,
    titleBarStyle: 'hiddenInset',
    frame: false,
    autoHideMenuBar: true,
    webPreferences
  })

  const startUrl = url.format({
    pathname: path.join(__dirname, `/../../dist/material-reader-comic-viewer/index.html`),
    protocol: 'file:',
    slashes: true
  })

  mainWindow.loadURL(startUrl)

  mainWindow.webContents.on('did-finish-load', () => {
    mainWindow && mainWindow.show()
  })

  mainWindow.webContents.on('new-window', (event, url) => {
    event.preventDefault()
    shell.openExternal(url)
  })

  mainWindow.on('closed', () => { // macOS
    mainWindow = null
  })

  const menuBuilder = new MenuBuilder(mainWindow)
  menuBuilder.buildMenu()

  const fileListener = new FileListener(mainWindow, currentOs, userData)
  fileListener.setListeners()
}