import * as os from 'os'

export class OsService {
    platforms = {
        WINDOWS: 'WINDOWS',
        MAC: 'MAC',
        LINUX: 'LINUX',
        SUN: 'SUN',
        OPENBSD: 'OPENBSD',
        ANDROID: 'ANDROID',
        AIX: 'AIX',
    }
    
    platformsNames = {
        win32: this.platforms.WINDOWS,
        darwin: this.platforms.MAC,
        linux: this.platforms.LINUX,
        sunos: this.platforms.SUN,
        openbsd: this.platforms.OPENBSD,
        android: this.platforms.ANDROID,
        aix: this.platforms.AIX,
    }

    currentPlatform = this.platformsNames[os.platform()]

    findHandlerOrDefault = (handlerName, dictionary) => {
        const handler = dictionary[handlerName]
        if (handler) {
            return handler
        }
        if (dictionary.default) {
            return dictionary.default
        }
        return () => null
    }

    byOS = this.findHandlerOrDefault.bind(null, this.currentPlatform)

    current = this.byOS({
        [this.platforms.MAC]: () => 'mac',
        [this.platforms.WINDOWS]: () => 'windows',
        [this.platforms.LINUX]: () => 'linux',
        default: 'other',
    })

    constructor () { }
}

export const OsFinder = new OsService()