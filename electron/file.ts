import * as fs from 'fs'
import * as path from 'path'
import {
  BrowserWindow,
  ipcMain
} from 'electron'
import { SettingFile, DefaultSettings } from './setting-file'

export class FileListener {
  mainWindow: BrowserWindow
  currentOs: string
  settingsFolder: string
  userSettings: SettingFile = DefaultSettings
  settingsFileName: string = 'reader-settings.json'
  
  /**
   * Constructor for the class
   * @param mainWindow 
   * @param currentOs 
   * @param settingsFolder 
   */
  constructor(mainWindow: BrowserWindow, currentOs: string, settingsFolder: string) {
    this.mainWindow = mainWindow
    this.currentOs = currentOs
    this.settingsFolder = settingsFolder
    this.checkSettingFile(settingsFolder)
  }

  /**
   * Create a setting file if one doesn't exist
   * @param settingsFolder
   */
  checkSettingFile (settingsFolder) {
    const settingsPath = path.join(settingsFolder, this.settingsFileName)
    this.readFile(settingsPath)
      .then((settings: SettingFile) => {
        if (settings) this.userSettings = settings
        else {
          const settingsString = JSON.stringify(DefaultSettings, null, 2)
          this.writeFile(settingsFolder, this.settingsFileName, settingsString)
        }
      })
  }

  /**
   * Write a `filename` file in `dirPath` folder containing dirPath `content`
   * @param dirPath 
   * @param filename 
   * @param content 
   */
  writeFile (dirPath, filename, content) {
    return new Promise((resolve, reject) => {
      fs.writeFile(path.join(dirPath, filename), content, 'utf8', (error) => {
        if (error) reject(error)
        else {
          console.log(`The file ${filename} was saved`)
          resolve("file created successfully!")
        }
      })
    }).catch((err) => { console.warn(err) })
  }

  /**
   * Read a file located at `filePath`
   * @param filePath 
   */
  readFile (filePath) {
    return new Promise((resolve, reject) => {
      fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) reject(err)
        else resolve(data)
      })
    }).catch((err) => { console.warn(err) })
  }

  /**
   * Read a file synchronously located at `filePath`
   * @param filePath 
   */
  readFileSync (filePath) {
    let readFile = null
    try {
      readFile = fs.readFileSync(filePath, 'utf8')
    } catch (err) {
      console.warn(err)
    }
    return readFile
  }

  /**
   * Get content of a directory located at `filePath`
   * @param filePath 
   */
  readDir (filePath) {
    return new Promise((resolve, reject) => {
      fs.readdir(filePath, (err, data) => {
        if (err) reject(err)
        else resolve(data)
      })
    }).catch((err) => { console.warn(err) })
  }

  /**
   * Get file stat of a file located at `filePath`
   * @param filePath 
   */
  getFileStat (directory, filename) {
    const filePath = path.join(directory, filename)
    return new Promise((resolve, reject) => {
      fs.lstat(filePath, (err, data) => {
        if (err) reject(err)
        else resolve({ stats: data, name: filename })
      })
    }).catch((err) => { console.warn(err) })
  }

  setListeners () {
    ipcMain.on('getFile', (event, arg) => {
      let readFile = null
      return this.readFile(arg)
        .then((file) => {
          readFile = file
        })
        .catch((err) => {
          console.warn(err)
        })
        .finally(() => {
          this.mainWindow.webContents.send('getFileResponse', readFile)
        })
    })

    ipcMain.on('getFiles', (event, arg) => {
      let readFiles = null
      return this.readDir(arg)
        .then((file) => {
          readFiles = file
        })
        .catch((err) => {
          console.warn(err)
        })
        .finally(() => {
          this.mainWindow.webContents.send('getFilesResponse', readFiles)
        })
    })

    ipcMain.on('getFileStats', (event, arg) => {
      let fileStats = null
      return this.getFileStat(arg.directory, arg.filename)
        .then((stats) => {
          fileStats = stats
        })
        .catch((err) => {
          console.warn(err)
        })
        .finally(() => {
          this.mainWindow.webContents.send('getFileStatsResponse', fileStats)
        })
    })

    ipcMain.on('getFilesStats', (event, arg) => {
      let statsPromises = []
      arg.forEach((pathInfo) => {
        statsPromises.push(this.getFileStat(pathInfo.directory, pathInfo.filename))
      })
      let fileStats = null
      return Promise.all(statsPromises)
        .then((stats) => {
          fileStats = stats
        })
        .catch((err) => {
          console.warn(err)
        })
        .finally(() => {
          this.mainWindow.webContents.send('getFilesStatsResponse', fileStats)
        })
    })

    ipcMain.on('minimize', (event, arg) => {
      this.mainWindow.minimize()
    })

    ipcMain.on('maximize', (event, arg) => {
      if (this.mainWindow.isMaximized()) {
        this.mainWindow.unmaximize()
      } else {
        this.mainWindow.maximize()
      }
    })

    ipcMain.on('close', (event, arg) => {
      this.mainWindow.close()
    })

    ipcMain.on('getOs', (event, arg) => {
      this.mainWindow.webContents.send('getOsResponse', this.currentOs)
    })

    ipcMain.on('getUserSettings', (event, arg) => {
      this.mainWindow.webContents.send('getUserSettingsResponse', this.userSettings)
    })
  }
}