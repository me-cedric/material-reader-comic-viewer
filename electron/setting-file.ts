import { Keymap } from './keymap'

export class SettingFile { // mirrored in angular models
    comicFolders: Array<string>
    keymap: Keymap
}

const DefaultSettings: SettingFile = {
    comicFolders: [],
    keymap: {
        previousPage: '',
        nextPage: '',
        zoomIn: '',
        zoomOut: '',
        search: ''
    }
}

export {DefaultSettings}